# Dependency scanner - Ansible provisioning

- [Introduction](#introduction)
- [Requirement](#requirements)
- [Provision your server](#provision-your-server)

Introduction
============

This Ansible playbook can be used to install the dependency scanner from the following project:
[https://gitlab.com/Guillaume695/dependency-scanner](https://gitlab.com/Guillaume695/dependency-scanner)

This playbook will install and configure the following packages:
- apache2
- mysql
- django and all the modules required by the dependency scanner

Requirements
============

To run this project, you only need ansible installed on your system. Follow the installation instructions on Ansible's website here:
[http://docs.ansible.com/ansible/latest/intro_installation.html](http://docs.ansible.com/ansible/latest/intro_installation.html)

Provision your server
=====================

#### 1. First, clone this GitLab repository.

```bash
$ git clone https://gitlab.com/Guillaume695/dependency-scanner-ansible
``` 

#### 2. Go to the project folder, edit the hosts file and add the ip address of your server. Here is an example:

```
[server]
192.168.0.115
``` 

#### 3. Complete vars/user.yml with your configuration. Here is an example:

```yml
---
# TODO: complete with your configuration

# This user will be used for installation (has to be in sudo group)
ssh_user: ''
ssh_user_pass: ''

# This mysql user will be created and then used by the dependency scanner to save data in depscan_db database
db_user: ''
db_user_pass: ''

# Give your current mysql root password, or set a new one
db_root_pass: ''
```

#### 4. Run the playbook with the following command (from the repository folder):

```bash
$ ansible-playbook -i hosts depscan-install.yml
```

#### 5. Navigate to http://[ip_of_your_server] with your web browser.